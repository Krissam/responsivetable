import React, { useState, useEffect, useRef } from 'react';
import './App.css';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import TableData from '../../repository/data-set/table.json';
import TableColumn from '../../repository/data-set/column.json';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ButtonToolbar, Button, Overlay, Popover } from 'react-bootstrap';

const App = () => {
  const [selectedAll, setSelectedAll] = useState(false);

  const getColumnWidth = (rows, accessor, headerText) => {
    const maxWidth = 400
    const magicSpacing = 10
    const cellLength = Math.max(
      ...rows.map(row => (`${row[accessor]}` || '').length),
      headerText.length,
    )
    return Math.min(maxWidth, cellLength * magicSpacing)
  }

  const RenderTable = (props) => {
    const [tableFilter, setTableFilter] = useState([]);
    const [tableCol, setTableCol] = useState(null);
    
    useEffect(() => {
      console.log('BUILD COLUMN DATA');
      let columnsData = [];
      TableColumn.forEach(element => {
        element.width = getColumnWidth(TableColumn, element.accessor, element.Header);
        if (element.accessor === 'Attachement') {
          element.width = 50;
          let iconHeader = (
            <React.Fragment>
              <i className="fas fa-bars"></i>
              <span className="float-right">
                <i className="fas fa-exchange-alt fa-rotate-90"></i>
              </span>
            </React.Fragment>
          );
          element.Header = iconHeader;

          element.Cell = ({ row }) => {
            if (row.Attachement === "TRUE") {
              return <i className="fas fa-check"></i>
            } 
            return <i className="fas fa-file"></i>
          };
        } else if (element.accessor === 'Liked') {
          element.Header = (
            <React.Fragment>
              <i className="fas fa-heart"></i>
              <span className="float-right">
                <i className="fas fa-exchange-alt fa-rotate-90"></i>
              </span>
            </React.Fragment>
          );
          element.width = 60;
        } else if (element.accessor === 'Year' 
        || element.accessor === 'Price') {
          element.width = 70;
          let iconHeader = (
            <React.Fragment>
              {element.Header}
              <span className="float-right">
                <i className="fas fa-exchange-alt fa-rotate-90"></i>
              </span>
            </React.Fragment>
          );
          element.Header = iconHeader;

        } else if (element.accessor === 'Reg') {
          element.width = 70;
          let iconHeader = (
            <React.Fragment>
              {element.Header}
              <span className="float-right">
                <i className="fas fa-exchange-alt fa-rotate-90"></i>
              </span>
            </React.Fragment>
          );
          element.Header = iconHeader;
        } else if (element.accessor === 'SalesRep') {
          element.width = undefined;
          element.sortable = true;
          let FilterHeader = <RowFilterOptionDom 
              tableData={TableData}
              element={element}
              header={element.Header}
              applyHandler={(data) => {
                setTableFilter(data);
              }}
            />;
          element.Header = FilterHeader;
        } else if (element.accessor === 'Make') {
          element.width = undefined;
          element.sortable = true;
          let FilterHeader = <RowFilterOptionDom 
              tableData={TableData}
              element={element}
              header={element.Header}
              applyHandler={(data) => {
                setTableFilter(data);
              }}
            />;
          element.Header = FilterHeader;
        } else if (element.accessor === 'Created' 
        || element.accessor === 'Updated'
        || element.accessor === 'Unpub'
        ) {
          element.width = 110;
          let iconHeader = (
            <React.Fragment>
              {element.Header}
              <span className="float-right">
                <i className="fas fa-exchange-alt fa-rotate-90"></i>
              </span>
            </React.Fragment>
          );
          element.Header = iconHeader;
        } else if (element.accessor === 'Views'
        || element.accessor === 'Leads') {
          element.width = 80;
          let iconHeader = (
            <React.Fragment>
              {element.Header}
              <span className="float-right">
                <i className="fas fa-exchange-alt fa-rotate-90"></i>
              </span>
            </React.Fragment>
          );
          element.Header = iconHeader;
        }


        
        columnsData.push(element);
      });

      const checkbox = {
        Header: () => (
          <input
            type="checkbox"
            className="checkbox"
            checked={(selectedAll === null) ? false : selectedAll}
            onChange={() => {
              setSelectedAll(!selectedAll);
            }}
          />
        ),
        Cell: row => (
          <RowCheckBoxDom
              checked={selectedAll}
          />
        ),
        accessor: 'Checkbox',
        width: 30
      }
      columnsData.unshift(checkbox);
      setTableCol(columnsData);
    },[]);

    return (
      <ReactTable
        showPagination={false}
        data={TableData}
        columns={(tableCol === null) ? [] : tableCol}
        defaultPageSize={10}
        minRows={10}
        filtered={[tableFilter]}
        onFilteredChange = {filtered => {
       
        }}
        defaultFilterMethod={(filter, row, column) => {
          const id = filter.pivotId || filter.id;
          if (typeof filter.value === "object") {
            return row[id] !== undefined
              ? filter.value.indexOf(row[id]) > -1
              : true;
          } else {
            return row[id] !== undefined
              ? String(row[id]).indexOf(filter.value) > -1
              : true;
          }
        }}
      />
    );

  }

  return (
    <RenderTable />
  );
}

export default App;

const RowCheckBoxDom = (props) => {
  const [selected, setSelected] = useState(false);

  useEffect(() => {
    setSelected(props.checked);
  },[props.checked]);

  useEffect(() => {
    if (
      'handleDataChange' in props 
      && 'name' in props
    ) {
      props.handleDataChange({
        "id": props.name,
        "value": selected
      });
    }
  },[selected]);

  return (
    <input
      type="checkbox"
      checked={selected}
      className="checkbox"
      onChange={() => {
        setSelected(!selected);
      }}
    />
  )
}

const RowFilterButtonDom = (props) => {
  const [show, setShow] = useState(false);
  const [target, setTarget] = useState(null);
  const ref = useRef(null);

  const handleClick = event => {
    setShow(!show);
    setTarget(event.target);
  };

  return (
    <React.Fragment>
      <span 
        onClick={handleClick}
        className="float-right"
      >
        <i className="fas fa-bars"></i>
      </span>
      <ButtonToolbar ref={ref}>
        <Overlay
          show={show}
          target={target}
          placement="bottom"
          container={ref.current}
          containerPadding={20}
        >
          <Popover id="popover-contained">
            <Popover.Content>
              <div className="filter-row-container">
              {props.children}
              </div>
            </Popover.Content>
          </Popover>
        </Overlay>
      </ButtonToolbar>
    </React.Fragment>
  );
}

const RowFilterOptionDom = (props) => {
  const [optionsList, setOptionsList] = useState([]);
  const [selectedAll, setSelectedAll] = useState(false);

  const TableData = props.tableData;
  let filterOption = [];
  let filterOptionDom = [];
  let filterItemDom = null;
  
  useEffect(() => {
    console.log('updated content', optionsList);
  },[optionsList]);

  const handleChange = (data) => {
    let exist = false;
    let tempOptionList = optionsList;

    tempOptionList.forEach((element, index) => {
      if (element.id == data.id) {
        if (data.value === false) {
          tempOptionList.splice(index, 1);
        }
        exist = true;
      }
    })

    if (exist === false && data.value === true) {
      tempOptionList.push(data);
    }

    setOptionsList(tempOptionList);
  }

  TableData.forEach((data, index)  => {
    if (!filterOption.includes(data[props.element.accessor])) {
      filterItemDom = (
        <label key={index} className="clickable item-filter">       
        <RowCheckBoxDom
          name={data[props.element.accessor]}
          checked={selectedAll}
          handleDataChange={handleChange}
        /> {data[props.element.accessor]}
        </label>
      );
      filterOptionDom.push(filterItemDom);
      filterOption.push(data[props.element.accessor]);
    }
  });

  return (
    <React.Fragment>
      {props.header}
      <RowFilterButtonDom>
        <label className="clickable">       
          SORT A TO Z
        </label>
        <label className="clickable">       
          SORT Z TO A
        </label>
        <label>       
          <strong>FILTER</strong>
        </label>
        <label className="clickable">       
          <input
            id="select-all-1"
            type="checkbox"
            className="checkbox"
            onChange={() => setSelectedAll(!selectedAll)}
          /> SELECT ALL
        </label>
        {filterOptionDom}
        <button 
          className="btn btn-block btn-outline-dark mt-2"
          onClick={() => {
            let tempConsolidatedFilter = [];
            optionsList.forEach((element, index) => {
              tempConsolidatedFilter.push(element.id);
            })

            props.applyHandler({
              "id": props.element.accessor,
              "value": tempConsolidatedFilter
            });
          }}
        >
          APPLY
        </button>
      </RowFilterButtonDom>
    </React.Fragment>
  );

}